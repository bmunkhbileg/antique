import React, { useState } from "react"
import { render } from "react-dom"
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import Fab from '@material-ui/core/Fab';
import CheckIcon from '@material-ui/icons/Check';
import SaveIcon from '@material-ui/icons/Save';
import { green } from '@material-ui/core/colors';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CircularProgress from '@material-ui/core/CircularProgress';
import SendIcon from '@material-ui/icons/Send';
import Routes from "../Routes";
import { Face, Fingerprint } from '@material-ui/icons'
import firebase from 'firebase/app'
import 'firebase/auth'
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        height: '100%',
        width: '100%',
        position: 'fixed',
        alignItems: 'center'
      },
      margin: {
        margin: theme.spacing.unit * 2,
        width: 370
    },
    padding: {
        padding: theme.spacing.unit
    }
  }));


function Login(props) {
    const classes = useStyles();

    const [checked, setChecked] = React.useState([1]);

    const [loading, setLoading] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const [email, setEmail] = React.useState('');
    const [pass, setPass] = React.useState(false);
    const timer = React.useRef();

    const buttonClassname = clsx({
      [classes.buttonSuccess]: success,
    });


    const handleButtonClick = () => {
      if (!loading) {
        setSuccess(false);
        setLoading(true);
        // props.history.push('/lobby')
        firebase.auth().signInWithEmailAndPassword(email, pass)
            .then((user)=> {
                console.log(user.user)
                firebase.auth().currentUser.getIdToken(true).then((res)=> {
                    console.log(res)
                })
            })
      }
    };
    
    return (
        <div className={classes.root}>
           <Paper className={classes.padding}>
               <input  type="file"
              id="input2"
              className="inputImg"
              onChange={(e) => uploadImage(e.target.files[0])} />
                <div className={classes.margin}>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Face />
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField onChange={e => setEmail(e.target.value)} id="username" label="Username" type="email" fullWidth autoFocus required />
                        </Grid>
                    </Grid>
                    <Grid container spacing={8} alignItems="flex-end">
                        <Grid item>
                            <Fingerprint />
                        </Grid>
                        <Grid item md={true} sm={true} xs={true}>
                            <TextField onChange={e => setPass(e.target.value)} id="username" label="Password" type="password" fullWidth required />
                        </Grid>
                    </Grid>
                    <Grid style={{ marginTop: '20px' }} container alignItems="center" justify="space-between">
                        <Grid item>
                            <FormControlLabel control={
                                <Checkbox
                                    color="primary"
                                />
                            } label="Remember me" />
                        </Grid>
                    </Grid>
                    <Grid container justify="center" style={{ marginTop: '10px' }}>
                        <Button onClick={()=> handleButtonClick()} variant="outlined" color="primary" style={{ textTransform: "none" }}>Login</Button>
                    </Grid>
                </div>
            </Paper>
        </div>
    )
}

export default Login;