import React, { useState } from "react"
import { render } from "react-dom"
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { green } from '@material-ui/core/colors';
import Routes from "../Routes";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexGrow: 1
      },
      appBar: {
        zIndex: theme.zIndex.drawer + 1,
        background: '#012669'
      },
      drawer: {
        width: drawerWidth,
        flexShrink: 0,
      },
      drawerPaper: {
        width: drawerWidth,
      },
      drawerContainer: {
        overflow: 'auto',
      },
      content: {
        flexGrow: 1,
        padding: theme.spacing(3),
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
      header: {
        justifyContent: 'space-between',
      },
      list: {
        width: '100%',
        maxWidth: 900,
        backgroundColor: theme.palette.background.paper,
      },
      inviteButton: {
        marginTop: '20px'
      },
      buttonSuccess: {
        backgroundColor: green[500],
        '&:hover': {
          backgroundColor: green[700],
        },
      },
      fabProgress: {
        color: green[500],
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
      },
      buttonProgress: {
        color: green[500],
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
      },
      wrapper: {
        margin: theme.spacing(1),
        position: 'relative',
      },
      buttonCont: {
        display: "flex",
        marginTop: '30px',
        alignItems: "center",
        width: 900,
        justifyContent: "flex-end"
      }
  }));


function Main(props) {
    console.log(props)
    const classes = useStyles();

    const [checked, setChecked] = React.useState([1]);

    const [loading, setLoading] = React.useState(false);
    const [success, setSuccess] = React.useState(false);
    const timer = React.useRef();

    const buttonClassname = clsx({
      [classes.buttonSuccess]: success,
    });

    React.useEffect(() => {
      return () => {
        clearTimeout(timer.current);
      };
    }, []);

    const handleToggle = (value) => () => {
      const currentIndex = checked.indexOf(value);
      const newChecked = [...checked];

      if (currentIndex === -1) {
        newChecked.push(value);
      } else {
        newChecked.splice(currentIndex, 1);
      }

      setChecked(newChecked);
    };

    const handleButtonClick = () => {
      if (!loading) {
        setSuccess(false);
        setLoading(true);
        props.history.push('/lobby')
        // inviteActions.invite({gameIDs: ['game1'], tID: 'Champs Daily DOTA2 Cup #100 (Premium Only)', roundID: 'round1', team1: 'HarYagaan', team2: 'sHs'})
        // .then((res)=> {
        //   console.log(res)
        //   setSuccess(true);
        //   setLoading(false);
        // })
      }
    };

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar className={classes.header}>
                    <Typography variant="h6" noWrap>
                        Dashboard
                    </Typography>
                    <Button color="inherit">Login</Button>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <Toolbar />
                    <div className={classes.drawerContainer}>
                        <List>
                            {['Бүтээгдэхүүн', 'Profile'].map((text, index) => (
                            <ListItem button key={text}>
                                <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                                <ListItemText primary={text} />
                            </ListItem>
                            ))}
                        </List>
                        <Divider />
                        <List>
                            {['Logout'].map((text, index) => (
                            <ListItem button key={text}>
                                <ListItemIcon>{index % 2 === 0 ? <ExitToAppIcon /> : <MailIcon />}</ListItemIcon>
                                <ListItemText primary={text} />
                            </ListItem>
                            ))}
                        </List>
                    </div>
            </Drawer>
            <main className={classes.content}>
                <Routes />
            </main>
        </div>
    )
}

export default Main;