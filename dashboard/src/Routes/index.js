import React, { Component } from "react";
import Home from "../Pages/home";


import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  
function Routes() {
    return (      
        <Switch>
            <Route exact path="/" component={Home} />
        </Switch>
    )
}
export default Routes;