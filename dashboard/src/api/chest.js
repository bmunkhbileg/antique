let chest = exports;
let chestList = {};

chest.set = function(data){
    chestList = {
        ...chestList,
        ...data
    }
};

chest.get = function(option){
    return chestList[option];
};