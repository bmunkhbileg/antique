import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const create = async (data) => {
    return networkHandler(
        urls.create,
        {
            method: "post",
            headers: header(false),
            body: JSON.stringify(data)
        }
    )
};


