import React, { useEffect, useState } from "react"
import './firebase/config'
import { render } from "react-dom"
import Button from '@material-ui/core/Button';
import Main from './Pages'
import Login from './Pages/login'
import './style/style.less'
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
  withRouter
} from "react-router-dom";
import { createBrowserHistory } from 'history';


function App() {
    const [state, setState] = useState("CLICK ME");
    
    return (
            <>
              <Route exact path="/" component={Main} />
            </>
    )
}

export default withRouter(App);

const wrapper = document.getElementById("root");
wrapper ? render(
<BrowserRouter>
  <App  history={createBrowserHistory}  />
</BrowserRouter>, 
wrapper) : false;