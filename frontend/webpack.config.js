const webpack = require("webpack");
const path = require("path");
const ASSET_PATH = process.env.ASSET_PATH || "/assets";
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const AssetsPlugin = require("assets-webpack-plugin");

let mode = process.env.NODE_ENV || "development";
if (!global._babelPolyfill) {
  require("babel-polyfill");
}


module.exports = {
  entry: ["babel-polyfill", path.join(__dirname, "src", "index.js")],
  output: {
    path: path.join(__dirname, "build"),
    filename: "[name].bundle.js",
    publicPath: "/",
  },
  devtool: mode === "development" ? "inline-source-map" : false,
  mode: process.env.NODE_ENV || "development",
  performance: {
    hints: "warning",
    // Calculates sizes of gziped bundles.
    assetFilter: function (assetFilename) {
      return assetFilename.endsWith(".js.gz");
    },
  },
  resolve: { modules: [path.resolve(__dirname, "src"), "node_modules"] },
  devServer: {
    host: "localhost",
    port: 3001,
    disableHostCheck: true,
    historyApiFallback: true,
    hot: false,
    inline: false,
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader",
          },
        ],
      },
      {
        test: /\.(sass|less|css)$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          {
            loader: "less-loader",
          },
          {
            loader: "style-resources-loader",
            options: {
              patterns: [path.resolve(__dirname, "src/style/variables.less")],
            },
          },
        ],
      },
      {
        test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
        loaders: ["file-loader"],
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "src", "index.html"),
    }),
    new CompressionPlugin({
      test: /\.js(\?.*)?$/i,
    }),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all",
        },
      },
    },
    runtimeChunk: {
      name: "manifest",
    },
  },
};

