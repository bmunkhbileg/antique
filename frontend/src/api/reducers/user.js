import { getUser,  getMyInfo } from "../constants";

const initialState = {
    user: [],
    myInfo: [],
    loadingUser:true,
    loadingMyInfo:true
};

export default function user(state = initialState, action){
    switch (action.type){
        case getUser.REQ:
            return {
                ...state,
                user: false,
                loadingUser: true,
            };
        case getUser.RES:
            switch (action.status){
                case 200:
                    return {
                        ...state,
                        user: action.payload,
                        loadingUser: false
                    };
                default:
                    return {
                        ...state,
                        user: action.status,
                        loadingUser: false
                    };
            }
        case getMyInfo.REQ:
            return {
                ...state,
                myInfo: false,
                loadingMyInfo: true,
            };
        case  getMyInfo.RES:
            switch (action.status){
                case 200:
                    return {
                        ...state,
                        myInfo: action.payload,
                        loadingMyInfo: false
                    };
                default:
                    return {
                        ...state,
                        myInfo: action.status,
                        loadingMyInfo: false
                    };
            }
        default:
            return state;
    }
}