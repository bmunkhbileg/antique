import { gameMode } from "../constants";

const initialState = {
    mode:'dota',
    findingMatch:''
};

export default function game(state = initialState, action){
    switch (action.type){
        case "GAME_MODE":
            return {
                ...state,
                mode: action.data.mode,
            };
        case "FINDING_MATCH":
            return  {
                ...state,
                findingMatch: action.data.matching,
            };
        default:
            return state;
    }
}
