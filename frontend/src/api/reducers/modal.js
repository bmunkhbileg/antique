import { openModal } from "../constants";

const initialState = {
    isOpen:false,
};

export default function modal(state = initialState, action){
    switch (action.type){
        case "OPEN_MODAL":
            return {
                ...state,
                isOpen: action.data.isOpen,
            };
    
        default:
            return state;
    }
}