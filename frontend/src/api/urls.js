const host = 'http://localhost:5000';
export const login = host + "/api/login";
export const createUser = host + "/api/register";
export const verifyRegister = host + "/api/verify_register";
export const forgotPassword = host + "/api/forgot_password";
