export function header(authorization){
    let token;
    if(JSON.parse(localStorage.getItem("User")) !== null) {
        token = JSON.parse(localStorage.getItem("User")).jwttoken       
    }
    if (authorization){
        return {
            "Content-Type": "application/json",
            "cache-control": "no-cache",
            "Authorization": "Bearer " + token
        };
    } else {
        return {
            "Content-Type": "application/json",
            "cache-control": "no-cache",
        };
    }
    
}