import * as urls from "../urls";
import { header } from "../header";
import networkHandler from "../fetcher/networkHandler";

export const userLogin = async (data) => {
    console.log(data)
    return networkHandler(
        urls.login,
        {
            method: "post",
            headers: header(false),
            body: JSON.stringify(data)
        }
    )
};

export const createUser = async (data) => {
    return networkHandler(
        urls.createUser,
        {
            method: "post",
            headers: header(false),
            body: JSON.stringify(data)
        }
    )
};

export const verifyRegister = async (data) => {
    return networkHandler(
        urls.verifyRegister,
        {
            method: "post",
            headers: header(false),
            body: JSON.stringify(data)
        }
    )
};

export const forgotPassword = async (data) => {
    return networkHandler(
        urls.forgotPassword,
        {
            method: "post",
            headers: header(false),
            body: JSON.stringify(data)
        }
    )
};

export const checkUsername = async (data) => {
    return networkHandler(
        urls.checkUsername,
        {
            method: "post",
            headers: header(false),
            body: JSON.stringify(data)
        }
    )
};
