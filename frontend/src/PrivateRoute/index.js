import React from 'react';
import { Route, Redirect } from 'react-router-dom';
let isLogin = false;

const PrivateRoute = ({component: Component, ...rest}) => {
    if(JSON.parse(localStorage.getItem("User")) !== null) {
        isLogin = true;
    }
    switch(rest.location.pathname) {
        case '/login':
            return (
                <Route {...rest} render={props => (
                    !isLogin ?
                        <Component {...props} />
                        : <Redirect to="/client" />
                )} />
            )
            break;
        case '/client':
            return (
                <Route {...rest} render={props => (
                    isLogin ?
                        <Component {...props} />
                        : <Redirect to="/login" />
                )} />
            )
            break;
        case '/register':
            return (
                <Route {...rest} render={props => (
                    !isLogin ?
                        <Component {...props} />
                        : <Redirect to="/client" />
                )} />
            )
            break;
        default:
            return (
                <Route {...rest} render={props => (
                    isLogin ?
                        <Component {...props} />
                        : <Redirect to="/login" />
                )} />
            )
    }
};

export default PrivateRoute;