import React, { Component } from "react";
import Main from "../Main";
import PrivateRoute from "../PrivateRoute"

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
  
function Routes() {
    return (      
        <Switch>
            <PrivateRoute exact path="/" component={Main} />
        </Switch>
    )
}
export default Routes;