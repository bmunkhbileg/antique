import React, { Component, useState, useEffect } from "react";
import Header from './Components/Header'
import LeftSide from './Components/leftSide'
import MainSide from './Components/mainSide'

import Routes from './Components/mainSide/Router'
import { Spinner } from 'react-activity';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

function Main() {

    const [user, setUser] = useState(null)
    const [loading, setLoading] = useState(true)

    useEffect(()=> {
        setLoading(false)
        
    }, [])

    return (
        <div>
            {
                loading ? (
                    <div className="loader">
                        <Spinner size={25} />
                    </div>
                ):(
                    <div>
                        <Header />
                        <LeftSide />
                        <MainSide />
                    </div>
                )
            }
        </div>
    )
}

export default Main;