import React, { Component,useState,useEffect } from "react";
import "./home.less";
import { IoMdArrowRoundForward } from "react-icons/io";
import { IoCalendar } from "react-icons/io5";
import { GiThreeFriends,GiSwordsEmblem } from "react-icons/gi";
import { Spinner } from "react-activity";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { IoTrophy } from "react-icons/io5";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const fs = firebase.firestore()

function Single(props) {
  const [count1, setCount1] = useState(0)
  const [data , setData] = useState({})
  const [loading,setLoading] = useState(true)
  const [user,setUser] = useState(null)

  const getData = async () => {
    console.log(props.location.state.id)
    let product = await firebase.firestore().collection('products').doc(props.location.state.id).get()
    setData(product.data())
  }

  useEffect(()=> {
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        console.log(user)
        setUser(user.uid)
      } else {
        // No user is signed in.
      }
    });
      getData()
  }, [props])  

  return (
    <div className="single">
        <div className="image">
          <img src={data.img} />
        </div>
        <div className="right">
            <div className="title">
              {data.name}
            </div>
            <div className="desc">
              {data.desc}
            </div>
            <div className="info">
              <span>{data.price}₮</span>
              <span>Хямдрал: 0₮</span>
              <span>Хэмнэлт: 0₮</span>
            </div>
            <div className="count">
              <span onClick={() => count1 > 0 ? setCount1(prevCount => prevCount - 1):null}>-</span>
              <span>{count1}</span>
              <span onClick={() => setCount1(prevCount => prevCount + 1)}>+</span>
            </div>
            <button onClick={()=> push(props.location.state.id)}>Сагсанд нэмэх</button>
        </div>
    </div>
  );
}

export default Single;
